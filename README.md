## Additional components for running scheduler

* to access the cluster from within
```
kubectl create clusterrolebinding serviceaccounts-cluster-admin --clusterrole=cluster-admin --group=system:serviceaccounts
```

* to access the cluster from outside using golang
```
// Create a client config and context
 var kubeconfig *string
 if home := homedir.HomeDir(); home != "" {
 kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "")
 } else {
 kubeconfig = flag.String("kubeconfig", "", "")
 }
 flag.Parse()

 config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
 ```
This inClsuter scheduler is deployed inside the minikube cluster as a pod. name: kube-scheduler-*   in default workspace

 * The result is displayed in the pdf file 


## change Description

commit id : 7dff1b67
* deployment.yaml --> 
            apiVersion,
            selector,
            liveness port,
            mountpath

* service.yaml -->
            targetport,
            selector


## kube-scheduler information:
 * deployment.yaml --> common deployment file without a service or a port, as it does not serve any request. This is deployed as an in-house scheduler to manage kubernetes cluster.

 * Flow of scheduler:
    - language : golang
    - main method calls the periodic method from file kubemaster.go
    - kubemaster.go has 5 functions ( 3 primary , 2 utilities )
    - setConfig() creates the context and config to access the cluster and execute the kube api methods (here the method is update by using the interface deployment)
    - kubemaster()  changes the deployment definition in the runtime according to the values passed down from the Periodic() function
    - Periodic() is an exported function which can be accessed outside the package api. 
    - Periodic() function uses the method NewTicker() previously Ticker
        The NewTicker() function in Go language is used to output a new Ticker that contains a channel in order to transmit the time with a period as stated by the duration parameter. It is helpful in setting the intervals or dropping the ticks of the ticker in order to make up for the slow recipients.
    - Periodic() function also makes use of channels to pass the ticker value to the getPodVal(), to find out the number of pods required to be scaled.
    - To prevent deadlock, there is a timeout value as well (timeout is not used, as that block is unreachable, due to the nature of this task to scale up and down deployment pods with time for an infinite amount of time.). In case of production systems, timeout is the best way to manage the channels.
    - Once pod value is determined, its passed down to kubemaster() to scale the deployment to the exact size. screen shot in pdf.
