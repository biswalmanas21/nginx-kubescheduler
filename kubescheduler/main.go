package main

import (
	"fmt"

	"github.com/kubescheduler/api"
)

func main() {
	fmt.Println("starting Kube scheduler...... : V.1.0.0")
	api.Periodic()
}
