package api

import (
	"context"
	//"flag"
	"fmt"
	"log"

	//"path/filepath"
	"time"

	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	client "k8s.io/client-go/kubernetes/typed/apps/v1"
	"k8s.io/client-go/rest"

	//"k8s.io/client-go/tools/clientcmd"
	//"k8s.io/client-go/util/homedir"
	"k8s.io/client-go/util/retry"
)

func converti32(i int32) *int32 {
	return &i
}

func setConfig() (deploymentClient client.DeploymentInterface) {
	// Create a client config and context
	// var kubeconfig *string
	// if home := homedir.HomeDir(); home != "" {
	// 	kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "")
	// } else {
	// 	kubeconfig = flag.String("kubeconfig", "", "")
	// }
	// flag.Parse()

	//config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	config, err := rest.InClusterConfig()

	if err != nil {
		log.Fatal(err)
	}
	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatal(err)
	}
	//create an deployment object
	deploymentClient = clientSet.AppsV1().Deployments(apiv1.NamespaceDefault)

	return
}

func kubemaster(podcount int, deploymentClient client.DeploymentInterface) (completed bool) {

	// update deployment
	fmt.Printf("\nReceived the number of pods from Periodic event : %v", podcount)
	// Strategy: get the deployment, modify it and preserve the state , use retry method
	deploymentName := "nginx-1600521727-nginx"
	retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {
		result, getErr := deploymentClient.Get(context.TODO(), deploymentName, metav1.GetOptions{})
		if getErr != nil {
			fmt.Println(getErr)
		}

		i32podcount := int32(podcount)
		//updating replicas
		result.Spec.Replicas = converti32(i32podcount)
		_, updateErr := deploymentClient.Update(context.TODO(), result, metav1.UpdateOptions{})
		return updateErr
	})
	if retryErr != nil {
		log.Fatal(retryErr)
	}

	fmt.Printf("\nDeployment updated successfully with pod count : %v", podcount)
	completed = true
	// send result
	return

}

// Periodic function is being referenced ut side api package in main method
func Periodic() {
	// find out current time till minutes
	deploymentClient := setConfig()
	// Clock hand ticker by interval
	ticker := time.NewTicker(1 * time.Minute)

	// close ticker once event is closed
	defer ticker.Stop()
	//even closure channel
	done := make(chan bool)

	//anonymous function to signal closing of events
	go func() {
		for {
			time.Sleep(5 * time.Minute)
		}

		// because of the for loop this block will be unreachable
		/* This timeout value is to prevent deadlock */
		done <- true
	}()

	// Catering incoming events
	for {
		select {
		case <-done:
			fmt.Printf("\nAll events closed")
			return
		case t := <-ticker.C:
			fmt.Printf("\nCurrent time found : %v \n", t)
			podcount := getPodVal(t)
			fmt.Printf("\nPod to be scaled to size %v \n", podcount)
			result := kubemaster(podcount, deploymentClient)
			if result == true {
				fmt.Printf("\nEvent Loop end . ========================================================\n")
			}

		}

	}

}

func getPodVal(t time.Time) (podCount int) {
	m := t.Minute()
	fmt.Printf("\ncurrent minute is %v\n", m)
	podCount = m % 10
	fmt.Printf("\nExtracted pod event number : %v\n", podCount)
	return
}
